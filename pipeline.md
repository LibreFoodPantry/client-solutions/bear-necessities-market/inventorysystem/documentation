# Pipeline

## Purpose

The purpose of this document is to reference sources for CI/CD pipelines within GitLab for the LFP project.

## References

### LFP-Based

[LibreFoodPantry on CI/CD](https://librefoodpantry.org/docs/ci-cd)

### Documentation on GitLab

[General Documentation](https://docs.gitlab.com/ee/ci/)

[Quick Start Guide](https://docs.gitlab.com/ee/ci/quick_start/)

[Docker Integration](https://docs.gitlab.com/ee/ci/docker/)

### CI/CD Template

[LFP Example Pipeline](https://gitlab.com/LibreFoodPantry/common-services/tools/pipeline)

This pipeline is the standard for both the InventorySystemBackend and the InventorySystemFrontend. All projects under LFP should be using this pipeline template as a base for standardization purposes.

**Note:** CI/CD is on by default, so if it's not working and you can't tell why, make sure to check your project settings to ensure CI/CD is still turned on.
