# Inventory System Documentation

DATE CHANGED:  November, 11, 2024

## Project Overview

This project contains the necessary files for running the Inventory System for Bear Necessities Market, used for keeping track of entries in the inventory and generating reports based on those entries.

This includes:

- The [InventorySystemFrontend](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/inventorysystem/frontend)
- The [InventorySystemBackend](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/inventorysystem/backend)
- The [InventorySystemIntegration](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/inventorysystem/inventorysystemintegration)

## Status

Currently, the project's Frontend and Backend both work on their own. Integration on the frontend has been completed, and a working version has not yet been deployed.

## Setting up the Development Environment

Currently, there are instructions in both the Frontend and Backend repositories on how to set up each respective part of the Inventory System.

## Running the Application

To run the system, information is found in the InventorySystemIntegration readme.

## Design

All design documents can be located in the [design folder](/design).

## Formatting

Formatting data from the inventory systems is found in the [format/data folder](/format/data).

## Scanner

How the scanner interacts with the system is explained in the [scanner folder](/scanner).

## NOTE

: These design documents are not representative of what the current system looks like. While the system may function in similar ways, it does not function exactly as illustrated in these documents.

These documents are only a rough outline of functionality, and should be updated when appropriate time is allotted to do so.
