# Future Development Suggestions

## Bugfixes

There may be unknown bugs as the testing environment was not functional during development of the MVP.

## Deployment

The current status of the project is MVP. It meets the minimum functionality as listed by the client and product owner, and therefore should be deployed as version 1.0 at your discretion. First test the system thoroughly, and if you encounter any breaking bugs, do not deploy before fixing.

## 'Products' collection - Backend/MongoDB

An additional collection for the database is recommended. This collection should store information about all previously encountered items from which to pull data for the inventory.

Product = {UPC, Name, Category, <any other data you deem relavent>}

It should have calls for GET, POST, PUT, DELETE.

## Scanner implementation - Frontend/Vue

This will depend on implementation of the 'Products' collection.

When opening an inventory session, the system should automatically wait for scanner input to gather the upc code. The upon scanning an item, one of the following situations should apply:
  - The UPC has not been encountered this session, but exists in the 'Products' collection: When scanning an item, the system should check if the upc exists in the 'Products' collection. If it does, it should automatically collect the name and any other data you want. Then the system should add it to the vue data list with a quantity of 1.
  - The item has already been scanned this session: If the upc code already exists in the vue data list, the quantity should be incremented by 1. This means that scanning an item with the same UPC code just means counting an additional item.
  - The UPC does not exist in the 'Products' collection: If the UPC does not exist in the 'Products' collection, a vue form should open allowing the user to input all the information for the product(as listed above). Upon submitting the form, this should be posted to the 'Product' collection, and then the item should be added to the vue data list with a quantity of 1.
Then the system should automatically wait for scanner input again.

Example for this model:
_After opening a new inventory session, the page immediately prompts me to scan an item. First I scan a can of chicken soup with UPC 012345678912. This is a new item that the market has never had in inventory before. When I scan it, a form opens asking for the item's name (Chicken Soup), the product category (Food), etc. I submit the form and the item appears on the page with a quantity of 1. I scan another chicken soup with the same UPC. This time the quantity just increments by one. Now I scan a pack of toilet paper which the market has had in inventory before. When I scan it, it appears on the page with a quantity of 1.
etc._


Notes:
After implementing this, input will no longer be needed for name and quantity, as those will be automatically filled (unless adding a new item to the 'Products' collection). This should be fixed in both frontend and backend.

## Checkout Session - Frontend/Vue

The quick entry function was introduced as a way to also be able to subtract inventory, as there was not one for the MVP. There should be a formal way to decrement the inventory in the form of a checkout session. It does not need to follow the model of Inventory Session, rather it should simply open a page that waits for scanner input. Scanning an item should simply subtract 1 from the quantity of that item in current inventory. Then it should wait for scanner input again. There should be an option on the page to indicate you are done and return to Home.

For this you should call PUT on inventory with the UPC and a quantity of -1.

## Testing - Backend

The testing environment for the system is not functional. This is located in the backend and should test all functionality and edge cases. It is recommended to scrap all of the current testing modules there except for 'calls.http' as it is all outdated. Rebuild it with the testing team.