# BNM Inventory System MVP Use Cases

Currently, there are two main requirements for the MVP of the Inventory System:

1. The client must be able to enter an item UPC code and quantity upon taking in new
stock or periodically taking inventory of existing stock.

2. The client must be able to generate a report of the inventory on a given date. The system will be supplied with a date and all inventory entries with that date will be aggregated and pulled into the report.
